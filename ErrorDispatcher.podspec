#
# Be sure to run `pod lib lint ErrorDispatcher.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ErrorDispatcher'
  s.version          = '0.1.8'
  s.summary          = 'Pod for error handling.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Pod for error handling. This pod implement chain of responsobilities pattern for handling errors in your app.
                       DESC

  s.homepage         = 'https://bitbucket.org/Khalbaevdm/errordispatcher'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Dmitriy Khalbaev' => 'dkhalbaev@dnomads.pro' }
  s.source           = { :git => 'https://bitbucket.org/Khalbaevdm/errordispatcher', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/KhalbaevDmitriy'

  s.ios.deployment_target = '9.0'
  s.swift_version = '4.2'

  # s.source_files = 'ErrorDispatcher/Classes/**/*'

  s.subspec 'Core' do |ss|
      ss.source_files = 'ErrorDispatcher/Classes/Core/**/*'
      ss.frameworks = 'Foundation'
      ss.dependency 'ErrorDispatcher/Alert'
  end

  s.subspec 'Alert' do |ss|
      ss.source_files = 'ErrorDispatcher/Classes/Alert/**/*'
      ss.frameworks = 'Foundation'
  end

  s.subspec 'ReactiveSwift' do |ss|
      ss.source_files = 'ErrorDispatcher/Classes/Reactive/**/*'
      ss.dependency 'ErrorDispatcher/Core'
      ss.dependency 'ReactiveSwift', '~> 5.0.0'
      ss.dependency 'Result', '~> 4.1'
  end

  # s.resource_bundles = {
  #   'ErrorDispatcher' => ['ErrorDispatcher/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
